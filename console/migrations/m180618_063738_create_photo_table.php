<?php

use yii\db\Migration;

/**
 * Handles the creation of table `homepage`.
 */
class m180618_063738_create_photo_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('homepage', [
            'id' => $this->primaryKey(),
            'url' => $this->string()->notNull(),
            'name' => $this->string(),
            'detail' => $this->string(),
            'date' => $this->integer(),
            'view' => $this->integer(),
            'camera' => $this->string(),
            'lens' => $this->string(),
            'focal_length' => $this->string(),
            'shutter_speed' => $this->string(),
            'iso' => $this->string(),
            'aperture' => $this->string(),
            'tripod' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('homepage');
    }
}
