<?php

namespace frontend\components\masonry;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

class MasonryWidget extends Widget
{

    public $items = [];

    public $options = [
        'widget' => [],
        'class' => []
    ];
// =========== OLD ===========
    public function init()
    {
        MasonryWidgetAsset::register($this->getView());
        Html::addCssClass($this->options, ['widget' => 'grid']);
    }
    public function run()
    {
        $items = [];
        for ($i = 0, $count = count($this->items); $i < $count; $i++) {

            $id = ArrayHelper::getValue($this->items, $i.'.id');
            $url = ArrayHelper::getValue($this->items, $i.'.url');

            $items[] = $this->renderItem($url,$id);
        }
        return Html::tag('div',implode("\n",$items),$this->options);
    }
    public function renderItem($url,$id)
    {
        $itemSrc = "<img src='$url'>";

        $link = Html::a(
            $itemSrc,
            ['photo/detial-photo'],
            [
                'data'=>[
                    'method' => 'post',
                    'params'=>[ 'id' => $id ],
                ]
            ]);

        return Html::tag('div', $link, ['class' => 'grid-item']);
    }
}