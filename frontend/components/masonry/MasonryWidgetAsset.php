<?php
/**
 * Created by PhpStorm.
 * User: MrChinnawat
 * Date: 14-Jun-18
 * Time: 00:36
 */

namespace frontend\components\masonry;

use yii\web\AssetBundle;
use Yii;

Yii::$app->assetManager->forceCopy = true;

class MasonryWidgetAsset extends AssetBundle
{
//    public $sourcePath = '@app/components/masonry';
    public $js = [
        '//unpkg.com/masonry-layout@4/dist/masonry.pkgd.js',
        '//unpkg.com/imagesloaded@4/imagesloaded.pkgd.js',
        'js/masonry.js'
    ];

    public $css = [
        'css/masonry.css'
    ];

    public $depends = [
        'yii\web\JqueryAsset'
    ];

    public function init()
    {
        $this->sourcePath = __DIR__ . "/asset";
        parent::init();
    }
}