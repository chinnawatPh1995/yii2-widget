<?php
use yii\helpers\ArrayHelper;

    $request = Yii::$app->request;

    $id = $request->post('id');

    $query = (new \yii\db\Query())
        ->select(['url', 'name'])
        ->from('photo')
        ->where(['id' => ''.$id])
        ->all();

    $getDetail = ArrayHelper::getValue($query, '0.url');
    ?>
    <div class="row">
        <div class="col-xs-12 col-md-9 col-lg-9 text-center" style="background-color: #000;">
            <img src="<?= $getDetail ?>" class="show-img" alt="">
        </div>
        <div class="col-lg-3"></div>
    </div>
