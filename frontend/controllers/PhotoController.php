<?php
/**
 * Created by PhpStorm.
 * User: Mr.Chinnawat
 * Date: 6/18/2018
 * Time: 5:23 PM
 */

namespace frontend\controllers;

use yii\web\Controller;
use frontend\models\Photo;

class PhotoController extends Controller
{
    public function actionDetialPhoto()
    {
        return $this->render('detailPhoto');
    }
}